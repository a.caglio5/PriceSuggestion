import os
import numpy as np
import joblib

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_log_error
from sklearn.metrics import mean_squared_error
from math import sqrt
from exploratory_analysis_utilities import pred_vs_true_label
from tensorflow import keras
from neural_network_utilities import rmsle


def evaluate(x_train, y_train, x_validation, y_validation, model):
    """
    Computes predictions and prints model's performances on training and validation set
    :param x_train: features of the instances of the train split
    :param y_train: labels of the instances of the train split
    :param x_validation: features of the instances of the validation split
    :param y_validation: labels of the instances of the validation split
    :param model: model used to compute predictions
    :return: predictions on training and validation set
    """
    # Performance on train set
    y_pred_train = model.predict(x_train)
    mse = mean_squared_error(y_train, y_pred_train)
    print('MSE value for train', mse)
    rmsle = sqrt(mean_squared_log_error(y_train, y_pred_train))
    print('RMSLE value for train', rmsle)

    # Performance on validation set
    y_pred_val = model.predict(x_validation)
    mse = mean_squared_error(y_validation, y_pred_val)
    print('MSE value for validation', mse)
    rmsle = sqrt(mean_squared_log_error(y_validation, y_pred_val))
    print('RMSLE value for validation', rmsle)

    return y_pred_train, y_pred_val


# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read numpy data
X_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
Y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')

X_train, X_validation, Y_train, Y_validation = train_test_split(X_train, Y_train, test_size=0.1, random_state=0)

# Load modes
neural_network = keras.models.load_model('models/neural_network', custom_objects={'rmsle': rmsle})
svm = joblib.load('models/svm.sav')
knn = joblib.load('models/knn.sav')

print("Neural Network")
y_pred_train_net, y_pred_val_net = evaluate(X_train, Y_train, X_validation, Y_validation, neural_network)
pred_vs_true_label(true=Y_train, pred=y_pred_train_net,
                   title='Neural Network - Train set',
                   save_path='results/prediction_plots/pred_vs_true_net_train.png')
pred_vs_true_label(true=Y_validation, pred=y_pred_val_net,
                   title='Neural Network - Validation set',
                   save_path='results/prediction_plots/pred_vs_true_net_val.png')
print("SVM")
y_pred_train_svm, y_pred_val_svm = evaluate(X_train, Y_train, X_validation, Y_validation, svm)
pred_vs_true_label(true=Y_train, pred=y_pred_train_svm,
                   title='SVM - Train set',
                   save_path='results/prediction_plots/pred_vs_true_svm_train.png')
pred_vs_true_label(true=Y_validation, pred=y_pred_val_svm,
                   title='SVM - Validation set',
                   save_path='results/prediction_plots/pred_vs_true_svm_val.png')
print("KNN")
y_pred_train_knn, y_pred_val_knn = evaluate(X_train, Y_train, X_validation, Y_validation, knn)
pred_vs_true_label(true=Y_train, pred=y_pred_train_knn,
                   title='KNN - Train set',
                   save_path='results/prediction_plots/pred_vs_true_knn_train.png')
pred_vs_true_label(true=Y_validation, pred=y_pred_val_knn,
                   title='KNN - Validation set',
                   save_path='results/prediction_plots/pred_vs_true_knn_val.png')
