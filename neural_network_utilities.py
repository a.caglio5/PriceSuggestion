import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras.backend as K
import time
import numpy as np

from keras import regularizers
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def model_definition(input_shape, activation, neurons, dropouts, regularizer=None):
    """
    Builds a Feedforward Neural Network based on the parameters given in input
    :param input_shape: number of features of the data that will be use to train the model
    :param activation: activation function name
    :param neurons: array of neurons (the number of values provided
     determines the number of layer of the neural network)
    :param dropouts: array that contains the dropout rate for each layer
    :param regularizer: definition of regularization
    :return: Feedforward Neural Network built with specific parameters
    """
    initializer = tf.keras.initializers.GlorotUniform(seed=1234)
    layer_list = [keras.Input(shape=input_shape)]

    for index in range(0, len(neurons)):
        layer_list.append(layers.Dense(neurons[index], activation=activation, kernel_initializer=initializer,
                                       kernel_regularizer=regularizer))
        if dropouts[index] > 0:
            layer_list.append(layers.Dropout(dropouts[index]))
    layer_list.append(layers.Dense(1, activation='linear'))

    return keras.Sequential(layer_list)


def rmsle(y_true, y_pred):
    """
    Computes Root Mean Squared Logarithmic Error between true labels and predicted labels
    :param y_true: true labels
    :param y_pred: predicted values
    :return: RMSLE between true and predicted labels
    """
    msle = tf.keras.losses.MeanSquaredLogarithmicError()
    return K.sqrt(msle(y_true, y_pred))


def plot_history(x_plot, model_history, epochs):
    """
    Plots model history during training in terms of model loss and model RMSLE
    :param x_plot: incremental integer array used as x axis of the plot
    :param model_history: history of a model fit
    :param epochs: number of epochs
    """
    plt.figure()
    plt.title('Model Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')

    plt.plot(x_plot, model_history.history['loss'])
    plt.plot(x_plot, model_history.history['val_loss'])
    plt.legend(['Training', 'Validation'])

    plt.xticks(range(1, epochs + 1))

    plt.figure()
    plt.title('Model Root Mean Squared Logarithmic Error')
    plt.xlabel('Epochs')
    plt.ylabel('Root Mean Squared Logarithmic Error')

    plt.plot(x_plot, model_history.history['rmsle'])
    plt.plot(x_plot, model_history.history['val_rmsle'])
    plt.legend(['Training', 'Validation'])

    plt.xticks(range(1, epochs + 1))

    plt.show()


def grid_search(x_train, y_train, batch_size, epochs, architectures,
                dropout_rates, weight_penalities, data_file=''):
    """
    Computes grid-search on Feedforward Neural Network using parameters given in input
    :param x_train: features of the training set instances
    :param y_train: labels of the training set instances
    :param batch_size: batch size value
    :param epochs: number of epochs
    :param architectures: array that defines the neural network architectures to try in the grid-search experiments
     (each value provided in the array corresponds to the number of neurons in a specific layer)
    :param dropout_rates: array that contains the dropout rates for each architecture
    :param weight_penalities: array of regularization weight penalties to try in the grid-search experiments
    :param data_file: if specified, path to save the grid-search results
    """
    print('*** GRID SEARCH ***')
    number_of_features = x_train.shape[1]
    x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train,
                                                                    test_size=0.1, random_state=0)
    count = 1
    number_of_experiment = len(architectures) * len(dropout_rates) * len(weight_penalities) * 3

    if data_file != '':
        with open(data_file, 'w') as logfile:
            logfile.write(
                'layers_number;neurons_per_layer;dropout_rates;'
                'weight_regularization;weight_penalit;train_loss;'
                'train_rmsle;validation_loss;validation_rmsle;'
                'train_eval_time\n'
            )

    # Iterate over each possible architecture
    # architectures is a list of list (each list contains the number of neurons for each layer)
    for neurons in architectures:
        # Iterate over each possible dropout rates
        for dropout_rate in dropout_rates:
            # Init dropouts array (one for each layer)
            dropouts = np.full(shape=len(neurons), fill_value=dropout_rate)
            # Iterate over each hyperparameter of regularization
            for weight_penality in weight_penalities:
                # Init different type of regularization with the right hyperparameter
                regularizer = [regularizers.l1(weight_penality),
                               regularizers.l2(weight_penality),
                               regularizers.l1_l2(weight_penality)]
                # Iterate over each different type of regularization
                for index_reg_type in range(0, len(regularizer)):
                    time_start = time.clock()
                    print('##################################################')
                    print('>>> Experiment', count, 'out of', number_of_experiment)
                    print('--------------------------------------------------')
                    print('Number of layers:', len(neurons))
                    print('Neurons per layer: ', end='')
                    print(neurons)
                    print('Dropout:', dropout_rate)
                    print('Regularizer l', index_reg_type + 1, ': ', weight_penality, sep='')
                    print('--------------------------------------------------')
                    model = model_definition(input_shape=number_of_features,
                                             activation='relu',
                                             neurons=neurons,
                                             dropouts=dropouts,
                                             regularizer=regularizer[index_reg_type])
                    model.compile(loss='mse', optimizer='adam', metrics=rmsle)
                    model_history = model.fit(x_train, y_train,
                                              batch_size=batch_size,
                                              epochs=epochs,
                                              verbose=0,
                                              validation_data=(x_validation, y_validation))
                    score_train = model.evaluate(x_train, y_train, verbose=0)
                    print('Train loss:', score_train[0])
                    print('Train RMSLE:', score_train[1])

                    score_validation = model.evaluate(x_validation, y_validation, verbose=0)
                    print('Validation loss:', score_validation[0])
                    print('Validation RMSLE:', score_validation[1])
                    count = count + 1
                    time_elapsed = (time.clock() - time_start)
                    print('\n-> Train and evaluate time:', time_elapsed, '\n')
                    if data_file != '':
                        with open(data_file, 'a') as logfile:
                            logfile.write(
                                str(len(neurons)) + ';' +
                                ' '.join([str(neuron) for neuron in neurons]) + ';' +
                                str(dropout_rate) + ';' +
                                'l' + str(index_reg_type + 1) + ';' +
                                str(weight_penality) + ';' +
                                str(score_train[0]) + ';' +
                                str(score_train[1]) + ';' +
                                str(score_validation[0]) + ';' +
                                str(score_validation[1]) + ';' +
                                str(int(time_elapsed)) +
                                '\n')


def plot_grid_search(results, hyperparameter, figsize=None):
    """
    Plots the grid-search results of specific hyperparameters
    :param results: results obtained by grid-search
    :param hyperparameter: string or list of the hyperparameters whose plot you want to produce
    :param figsize: size of the plot
    """
    grouped_results = results.groupby(hyperparameter)[['validation_loss',
                                                       'validation_rmsle',
                                                       'train_loss',
                                                       'train_rmsle']].mean()
    if type(hyperparameter) == list:
        hyperparameter = '-'.join([str(s) for s in hyperparameter])
        group_index = []
        for index in list(grouped_results.index):
            tmp_string = ''
            tmp_string += index[0]
            tmp_string += '-'
            tmp_string += str(index[1])
            group_index.append(tmp_string)
    else:
        group_index = list(grouped_results.index)

    plt.figure(figsize=figsize)
    plt.title('Grid Search - Loss (' + hyperparameter + ')')
    plt.xlabel(hyperparameter)
    plt.ylabel('MSE')
    if (type(group_index[0]) != float) and (type(group_index[0]) != int):
        plt.xticks(range(len(group_index)), group_index, rotation=90)

    plt.plot(group_index, grouped_results['train_loss'].values)
    plt.plot(group_index, grouped_results['validation_loss'].values)
    plt.legend(['Training', 'Validation'])

    plt.figure(figsize=figsize)
    plt.title('Grid Search - RMSLE (' + hyperparameter + ')')
    plt.xlabel(hyperparameter)
    plt.ylabel('RMSLE')
    if (type(group_index[0]) != float) and (type(group_index[0]) != int):
        plt.xticks(range(len(group_index)), group_index, rotation=90)

    plt.plot(group_index, grouped_results['train_rmsle'].values)
    plt.plot(group_index, grouped_results['validation_rmsle'].values)
    plt.legend(['Training', 'Validation'])

    plt.show()


def layers_weight_inspection(model):
    """
    Inspects the weights of the layers
    :param model: model whose you want to ispect weights
    """
    print("layers_weight_inspection")
    for layer in model.layers:
        if 'dropout' not in layer.name:
            print("layer", layer.name)
            print("shape", layer.weights[0].shape)
            print('Sum of the weights:', sum(sum(abs(layer.weights[0]))).numpy())
