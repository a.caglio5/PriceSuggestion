import os
import pandas as pd

from preprocess_utilities import fill_NA, sep_category_name, prep_text, features_encoding

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

"""PRE-PROCESS DATASET"""

train = pd.read_csv(dataset_folder + 'train.tsv', sep='\t', header=0, encoding='ISO-8859-2')
test = pd.read_csv(dataset_folder + 'test.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Fill missing values
train = fill_NA(train, file_name='na_train_count')
test = fill_NA(test, file_name='na_test_count')

# Separate category features
train['category_1'], train['category_2'], train['category_3'] = sep_category_name(train)
test['category_1'], test['category_2'], test['category_3'] = sep_category_name(test)

# Item description and name preprocessing
train['name'] = train['name'].apply(lambda t: prep_text(t))
test['name'] = test['name'].apply(lambda t: prep_text(t))
train['item_description'] = train['item_description'].apply(lambda t: prep_text(t))
test['item_description'] = test['item_description'].apply(lambda t: prep_text(t))

# Add new features with number of word about item description and name
train['words_count_name'] = train['name'].apply(lambda t: len(t.split()))
test['words_count_name'] = test['name'].apply(lambda t: len(t.split()))
train['words_count_item_description'] = train['item_description'].apply(lambda t: len(t.split()))
test['words_count_item_description'] = test['item_description'].apply(lambda t: len(t.split()))

# Encoding categorical features
features_encoding(train, ['category_name', 'category_1', 'category_2', 'category_3', 'brand_name'])
features_encoding(test, ['category_name', 'category_1', 'category_2', 'category_3', 'brand_name'])

# Save preprocessed data (train and test) to csv
train.to_csv(dataset_folder + 'train_preprocessed.tsv', sep='\t', index=False)
test.to_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', index=False)
