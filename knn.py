import os
import numpy as np
import joblib
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn import neighbors

"""
IMPORT DATA:
At the first time, you must run the 'preprocess.py' and 'exploratory_analysis.py' files
"""

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed test data
test = pd.read_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')
x_test = np.load(dataset_folder + 'numpy_data/x_test.npy')

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

print("KNN REGRESSION")

k = 19

# Create a knn model
model = neighbors.KNeighborsRegressor(n_neighbors=k)

# Train the model using the training set
model.fit(x_train, y_train)

# Save the model
joblib.dump(model, 'models/knn.sav')

# Prediction on test set
y_test = model.predict(x_test)
y_test = np.expm1(y_test)
test = test.assign(price=y_test)
test.to_csv('results/test_with_label_knn.tsv', sep='\t', index=False)
