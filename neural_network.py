import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Hide debug and warning message of TensorFlow
import pandas as pd
import numpy as np

from tensorflow import keras
from keras import regularizers
from sklearn.model_selection import train_test_split
from neural_network_utilities import model_definition, plot_history, rmsle, layers_weight_inspection

"""
IMPORT DATA:
At the first time, you must run the 'preprocess.py' and 'exploratory_analysis.py' files
"""

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed test data
test = pd.read_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')
x_test = np.load(dataset_folder + 'numpy_data/x_test.npy')

"""MODEL DEFINITION"""

number_of_features = x_train.shape[1]

model_without_regularization = model_definition(input_shape=number_of_features,
                                                activation='relu',
                                                neurons=[512, 256, 128],
                                                dropouts=[0, 0, 0])

model = model_definition(input_shape=number_of_features,
                         activation='relu',
                         neurons=[512, 256, 128],
                         dropouts=[0, 0, 0],
                         regularizer=regularizers.l2(0.001))
model.summary()

"""MODEL TRAINING"""

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

batch_size = 128
epochs = 20

optimizer = keras.optimizers.Adam(learning_rate=0.0001)

model_without_regularization.compile(loss='mse', optimizer=optimizer, metrics=rmsle)
model_without_regularization_history = model_without_regularization.fit(x_train, y_train,
                                                                        batch_size=batch_size,
                                                                        epochs=epochs,
                                                                        verbose=1,
                                                                        validation_data=(x_validation, y_validation))

model.compile(loss='mse', optimizer=optimizer, metrics=rmsle)
model_history = model.fit(x_train, y_train,
                          batch_size=batch_size,
                          epochs=epochs,
                          verbose=1,
                          validation_data=(x_validation, y_validation))

# Plot loss and Root Mean Squared Error on training and validation set
x_plot = list(range(1, epochs + 1))
plot_history(x_plot, model_history, epochs)

"""SAVE MODEL"""

model_without_regularization.save(current_directory + '/models/neural_network_without_regularization')
model.save(current_directory + '/models/neural_network')

"""WEIGHT INSPECTION"""

print("MODEL WITHOUT REGULARIZATION")
layers_weight_inspection(model_without_regularization)

print("MODEL WITH REGULARIZATION")
layers_weight_inspection(model)

"""MODEL PREDICTION"""

# Prediction on test set
y_test = model.predict(x_test)
y_test = np.expm1(y_test)
test = test.assign(price=y_test)
test.to_csv('results/test_with_label_neural_network.tsv', sep='\t', index=False)
