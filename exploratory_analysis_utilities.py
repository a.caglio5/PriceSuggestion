import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


def distribution_of_continuous_feature(feature, dataset, bins, size=(10, 10)):
    """
    Plots the distribution of a continuous feature
    :param feature: name of the feature
    :param dataset: dataset that contains the feature whose plot you want to generate
    :param bins: number of bins of the histogram
    :param size: size of the plot
    :return: pandas series that contains the description of the feature properties
    """
    update_text_size(small_size=18, medium_size=23, big_size=23)
    desc = dataset[feature].describe()
    plt.figure(figsize=size)
    plt.hist(dataset[feature], bins)
    plt.xlabel(feature)
    plt.ylabel("Count")
    plt.show()
    return desc


def distribution_of_categorical_feature(feature, dataset, top_n=0, size=(10, 10)):
    """
    Plots the distribution of a categorical feature
    :param feature: name of the feature
    :param dataset: dataset that contains the feature whose plot you want to generate
    :param top_n: if specified, the plot will contain only the top_n values
    :param size: size of the plot
    :return: pandas series that contains the occurrences of the different values of the features
    """
    update_text_size(small_size=10, medium_size=23, big_size=23)
    count = dataset[feature].value_counts()
    fig, ax = plt.subplots(figsize=size)
    if top_n > 0:
        ax = sns.barplot(x=count[0:top_n].index, y=count[0:top_n].values, ax=ax, edgecolor='k', linewidth=0.5)
        ax.set_title("Top " + str(top_n) + " " + feature)
    else:
        ax = sns.barplot(x=count.index, y=count.values, ax=ax, edgecolor='k', linewidth=0.5)
        ax.set_title(feature + " Distribution")
    ax.set(xlabel="Count", ylabel=feature)
    if (feature == "category_name") or (feature == "brand_name"):
        plt.xticks(rotation=90)
    plt.show()
    return count


def correlation_matrix(x_train, y_train, size=(10, 10)):
    """
    Plots the correlation matrix of the features
    :param x_train: features of the instances
    :param y_train: labels of the instances
    :param size: size of the plot
    """
    update_text_size(small_size=18, medium_size=23, big_size=23)
    train_with_price = x_train.assign(price=y_train)
    plt.figure(figsize=size)
    cor = train_with_price.corr()
    sns.heatmap(cor, annot=False, cmap=plt.cm.Reds)
    plt.show()


def pred_vs_true_label(true, pred, title, save_path=''):
    """
    Plots the regression line using true and predicted values of the labels
    :param true: true labels
    :param pred: predicted labels
    :param title: title of the plot
    :param save_path: if specified, path to save the plot image
    """
    update_text_size(small_size=15, medium_size=19, big_size=19)
    true = np.expm1(true)
    pred = np.expm1(pred)
    plt.axes(aspect='equal')
    plt.scatter(x=true, y=pred, s=2)
    plt.xlabel('True Values [price]')
    plt.ylabel('Predictions [price]')
    plt.title(label=title)
    lims = [0, 1500]
    plt.xlim(lims)
    plt.ylim(lims)
    if save_path != '':
        plt.savefig(save_path)
    plt.show()


def update_text_size(small_size, medium_size, big_size):
    """
    Updates the text dimension of plot elements using three different sizes
    :param small_size: first size (small)
    :param medium_size: second size (medium)
    :param big_size: third size (big)
    """
    plt.rc('font', size=small_size)  # controls default text sizes
    plt.rc('axes', titlesize=medium_size)  # fontsize of the axes title
    plt.rc('axes', labelsize=medium_size)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=small_size)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=small_size)  # fontsize of the tick labels
    plt.rc('legend', fontsize=small_size)  # legend fontsize
    plt.rc('figure', titlesize=big_size)  # fontsize of the figure title
