import os
import smogn
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Data directory
from preprocess_utilities import data_normalization

current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed data
train = pd.read_csv(dataset_folder + 'train_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Removing unused features
train = train.drop(columns=['train_id', 'name', 'item_description',
                            'category_name', 'category_name_encoded',
                            'category_1', 'category_2',
                            'category_3', 'brand_name'])

# Sample a random portion of dataset
train = train.sample(frac=0.01, replace=False, random_state=1)
train = train.reset_index()
train = train.drop(columns='index')

# Apply data augmentation
train_smogn = smogn.smoter(data=train, y='price')

print('Original train shape:', train.shape)
print('SMOGN train shape:', train_smogn.shape)

print('Original train:', smogn.box_plot_stats(train['price'])['stats'])
print('SMOGN train', smogn.box_plot_stats(train_smogn['price'])['stats'])

# Plot density of dataset before and after augmentatin
sns.kdeplot(train['price'], label="Original")
sns.kdeplot(train_smogn['price'], label="Modified")
plt.show()

"""SAVE SMOGN DATASET TO NUMPY"""

# Divide features and labels
x_train_smogn = train_smogn.drop(columns=['price'])
y_train_smogn = pd.DataFrame(train_smogn['price'])

# Normalization of train features
x_train_smogn, _ = data_normalization(x_train_smogn)

# Log-normalization of price (target variable)
y_train_smogn['price'] = np.log1p(y_train_smogn['price'])

np.save(dataset_folder + 'numpy_data/x_train_14k_smogn', x_train_smogn)
np.save(dataset_folder + 'numpy_data/y_train_14k_smogn', y_train_smogn)

"""SAVE DATASET TO NUMPY"""

# Divide features and labels
x_train = train.drop(columns=['price'])
y_train = pd.DataFrame(train['price'])

# Normalization of train features
x_train, _ = data_normalization(x_train)

# Log-normalization of price (target variable)
y_train['price'] = np.log1p(y_train['price'])

np.save(dataset_folder + 'numpy_data/x_train_14k', x_train)
np.save(dataset_folder + 'numpy_data/y_train_14k', y_train)
