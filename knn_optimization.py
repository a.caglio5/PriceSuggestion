import os
import time
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn import neighbors
from sklearn.metrics import mean_squared_log_error
from sklearn.metrics import mean_squared_error
from math import sqrt


def optimize_k(x_train, y_train, x_validation, y_validation, k_max):
    """
    Prints k-NN performances to find the best k to use, trying its value in the range (1, k_max)
    :param x_train: features of the instances of the train split
    :param y_train: labels of the instances of the train split
    :param x_validation: features of the instances of the validation split
    :param y_validation: labels of the instances of the validation split
    :param k_max: maximum k in the range for testing k values
    """
    print('*** OPTIMIZE K ***')
    rmsle_train = []
    rmsle_val = []
    for k in range(k_max):
        time_start = time.clock()
        k = k + 1
        print('##################################################')
        print('>>> Experiment', k, 'out of', k_max, '( K = ', k, ')')
        print('--------------------------------------------------')
        model = neighbors.KNeighborsRegressor(n_neighbors=k)
        model.fit(x_train, y_train)

        pred = model.predict(x_train)
        rmsle = sqrt(mean_squared_log_error(y_train, pred))
        rmsle_train.append(rmsle)
        print('RMSLE value for train with k =', k, 'is:', rmsle)
        mse = mean_squared_error(y_train, pred)
        print('MSE value for train with k =', k, 'is:', mse)
        print('--------------------------------------------------')
        pred = model.predict(x_validation)
        rmsle = sqrt(mean_squared_log_error(y_validation, pred))
        rmsle_val.append(rmsle)
        print('RMSLE value for validation with k =', k, 'is:', rmsle)
        mse = mean_squared_error(y_validation, pred)
        print('MSE value for validation with k =', k, 'is:', mse, '\n')
        time_elapsed = (time.clock() - time_start)
        print('\n-> Train and evaluate time:', time_elapsed, '\n')

    rmsle_t = np.array(rmsle_train)
    rmsle_v = np.array(rmsle_val)
    print("Min train", np.amin(rmsle_t))
    print("Min validation", np.amin(rmsle_v))


"""
IMPORT DATA:
At the first time, you must run the 'preprocess.py' and 'exploratory_analysis.py' files
"""

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

optimize_k(x_train, y_train, x_validation, y_validation, 20)
