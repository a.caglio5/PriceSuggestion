import string

from sklearn.preprocessing import StandardScaler
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords


def data_normalization(x, scaler=None):
    """
    Computes data normalization
    :param x: data to normalize
    :param scaler: standard scaler object
    :return: data normalized and standardization
    """
    if not scaler:
        scaler = StandardScaler()
        scaler.fit(x)
    x = scaler.transform(x)
    return x, scaler


def fill_NA(x, file_name=''):
    """
    Fills missing values
    :param x: date whose missing values you want to find and fill
    :param file_name: if specified, path to save the results of NA values inspection
    :return: data corrected by filling values
    """
    if file_name != '':
        x.isna().sum().to_csv('results/preprocess/' + file_name + '.csv', sep=';', header=False)
    x['category_name'].fillna(value="Other/Other/Other", inplace=True)
    x['brand_name'].fillna(value="Unknown", inplace=True)
    x['name'].fillna(value="", inplace=True)
    x['item_description'].fillna(value="", inplace=True)
    return x


def sep_category_name(x):
    """
    Separates the features 'category_name' in 'category_1', 'category_2' and 'category_3'
    :param x: data whose features 'category_name' you want to separate
    :return: data with 'category_name' divided in three parts
    """
    categories_list = x['category_name'].str.split('/')
    category_1, category_2, category_3 = [], [], []
    for index in categories_list.index:
        category_1.append(categories_list[index][0])
        category_2.append(categories_list[index][1])
        category_3.append(categories_list[index][2])
    return category_1, category_2, category_3


def feature_encoding(x, column_name):
    """
    Encodes single categorical features
    :param x: data to encode
    :param column_name: categorical feature to encode
    :return: categorical feature encoded
    """
    return x[column_name].astype('category').cat.codes


def features_encoding(x, columns_list):
    """
    Encodes categorical features
    :param x: data to encode
    :param columns_list: list of categorical features to encode
    """
    for column_name in columns_list:
        x[column_name + '_encoded'] = feature_encoding(x, column_name)


def prep_text(text):
    """
    Computes preprocess on text
    :param text: text to preprocess
    :return: text preprocessed
    """
    stop_words = stopwords.words('english')
    punctuation = string.punctuation
    review_res = ''.join(c for c in text if not c.isnumeric())
    tokening = TweetTokenizer()
    review_list = tokening.tokenize(review_res)
    review_tokenized_stop_punct = [item for item in review_list if item not in stop_words]
    review_tokenized_stop_punct = [item for item in review_tokenized_stop_punct if item not in punctuation]
    review_tokenized_stop_punct = [each_string.lower() for each_string in review_tokenized_stop_punct]
    return ' '.join(review_tokenized_stop_punct)
