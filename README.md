# Mercari Price Suggestion

## Autori

* Andrea Caglio - 816762 - a.caglio5@campus.unimib.it
* Andrea Ponti - 816311 - a.ponti5@campus.unimib.it
* Simone Rizza - 816298 - s.rizza7@campus.unimib.it

## Struttura del progetto

Il progetto è costituito da un insieme di file **python**. Essi sono tutti localizzati nella `root` del progetto ed il loro nome è esplicativo delle operazioni di cui si occupano. Maggiori informazioni relative alla loro esecuzione, sono fornite nelle successive sezioni.  
Oltre ai file ai file **python**, il progetto presenta tre folder:
* `dataset`: contiene il dataset relativo all'e-commerce **Mercari**, reperibile alla seguente competizione *Kaggle*:
https://www.kaggle.com/c/mercari-price-suggestion-challenge  
Nella folder sono presenti anche i dati dei prodotti, ottenuti dall'operazione di preprocess. Per esigenze progettuali, le istanze dei prodotti sono state salvate anche in formato *numpy*, separando in file differenti le features dalle label. 
* `models`: contiene la definizione di tutti i modelli allenati nel progetto (rete neurale, *k*-NN e SVM).
* `results`: contiene i risultati prodotti in fase di preprocess, grid-search, ottimizzazione del *k*-nn e predizione.

## Configurazione del progetto

1. Per clonare il repository in locale, aprire il terminale e lanciare il seguente comando:

	    git clone https://gitlab.com/a.caglio5/PriceSuggestion.git

2. In questo modo si ha il repository in locale. Per entrare nella folder del progetto, lanciare il comando:

	    cd PriceSuggestion

3.  **Nota**
    
    Effettuando il clone del repository, le esecuzioni dei file Python che consentono di realizzare il preprocess dei dati, la grid-search per la rete neurale, l'ottimizzazione del *k*-NN e la data augmentation, non sono necessarie. A fini informativi, i passaggi necessari ad effettuare queste operazioni, sono riportati di seguito:

    1. Per effettuare la generazione dei file `train_preprocessed.tsv` e `test_preprocessed.tsv` è necessario creare una Run Configuration Python, specificando come Script path `preprocess.py`. In alternativa è possibile lanciare da terminale il comando:

            python preprocess.py
    
    2. Per produrre i grafici dell'analisi esplorativa e generare i file `x_train.npy`, `y_train.npy` e `x_test.npy` è necessario creare una Run Configuration Python, specificando come Script path `exploratory_analysis.py`. In alternativa è possibile lanciare da terminale il comando:

            python exploratory_analysis.py
            
    3. Per realizzare la grid-search, andando ad identificare gli iperparametri per una rete neurale ottimale, è necessario creare una Run Configuration Python, specificando come Script path `grid_search.py`. In alternativa è possibile lanciare da terminale il comando:

            python grid_search.py
            
    4. Per individuare il *k* ottimale per il *k*-NN, è necessario creare una Run Configuration Python, specificando come Script path `knn_optimization.py`. In alternativa è possibile lanciare da terminale il comando:

            python knn_optimization.py
    
    5. Per generare i dati a cui è stata applicata la data augmentation SMOGN, è necessario creare una Run Configuration Python, specificando come Script path `data_augmentation.py`. In alternativa è possibile lanciare da terminale il comando:

            python data_augmentation.py
## Esecuzione del progetto

1. Per la creazione della rete neurale e per la predizione del prezzo delle istanze di test, usando tale modello, è necessario creare una Run Configuration Python, specificando come Script path `neural_network.py`. In alternativa è possibile lanciare da terminale il comando:

        python neural_network.py

2. Per la creazione del SVM e per la predizione del prezzo delle istanze di test, usando tale modello, è necessario creare una Run Configuration Python, specificando come Script path `svm.py`. In alternativa è possibile lanciare da terminale il comando:

        python svm.py

3. Per la creazione del *k*-NN e per la predizione del prezzo delle istanze di test, usando tale modello, è necessario creare una Run Configuration Python, specificando come Script path `knn.py`. In alternativa è possibile lanciare da terminale il comando:

        python knn.py

4. Per produrre i plot relativi alle rette di regressione e per calcolare il Mean Squared Error (MSE) e il Root Mean Squared Logarithmic Error (RMSLE) della rete neurale, di SVM e di *k*-NN, è necessario creare una Run Configuration Python, specificando come Script path `models_evaluations.py`. In alternativa è possibile lanciare da terminale il comando:

        python models_evaluations.py
        
5. Per la creazione del modello di ensemble (basato su rete neurale, SVM e *k*-NN) e per la predizione del prezzo delle istanze di test, usando tale modello, è necessario creare una Run Configuration Python, specificando come Script path `ensemble.py`. In alternativa è possibile lanciare da terminale il comando:

        python ensemble.py   