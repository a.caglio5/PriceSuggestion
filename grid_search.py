import numpy as np
import pandas as pd
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Hide debug and warning message of TensorFlow

from neural_network_utilities import grid_search, plot_grid_search

"""
IMPORT DATA:
At the first time, you must run the 'preprocess.py' and 'exploratory_analysis.py' files
"""

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')

"""GRID SEARCH"""

grid_search(x_train=x_train, y_train=y_train,
            batch_size=128, epochs=20,
            architectures=[[16, 16], [32, 32]],
            dropout_rates=[0.1, 0.2, 0.3],
            weight_penalities=[0.01, 0.001],
            data_file='results/grid_search/models_performances_COGNOMENUMERO.csv')

""""SHOW RESULTS"""

results = pd.read_csv('results/grid_search/models_performances.csv', header=0, sep=';')
plot_grid_search(results, 'layers_number')
plot_grid_search(results, 'neurons_per_layer')
plot_grid_search(results, 'dropout_rates')
plot_grid_search(results, ['weight_regularization', 'weight_penalit'])
