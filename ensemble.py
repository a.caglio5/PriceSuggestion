import numpy as np
import os
import joblib
import pandas as pd

from tensorflow import keras
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_log_error
from math import sqrt
from exploratory_analysis_utilities import pred_vs_true_label
from neural_network_utilities import rmsle
from sklearn.metrics import mean_squared_error


def prediction(dataset):
    """
    Computes price prediction on specific dataset
    :param dataset: dataset to use for prediction
    :return: dataframe that contains prediction computed by neural network, SVM, k-NN and theirs ensemble
    """
    dataframe = pd.DataFrame()
    dataframe["neural_network_pred"] = neural_network.predict(dataset).flatten()
    dataframe["svm_pred"] = svm.predict(dataset).flatten()
    dataframe["knn_pred"] = knn.predict(dataset).flatten()

    dataframe['ensemble_pred'] = dataframe.mean(axis=1)

    return dataframe


# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed test data
test = pd.read_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')
x_test = np.load(dataset_folder + 'numpy_data/x_test.npy')

# Split train and validation
x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

# Load modes
neural_network = keras.models.load_model('models/neural_network', custom_objects={'rmsle': rmsle})
svm = joblib.load('models/svm.sav')
knn = joblib.load('models/knn.sav')

# Predictions on train set
predictions_train = prediction(x_train)
pred_vs_true_label(true=y_train, pred=predictions_train['ensemble_pred'],
                   title='Ensemble - Train set',
                   save_path='results/prediction_plots/pred_vs_true_ensemble_train.png')
rmsle_metric = sqrt(mean_squared_log_error(y_train, predictions_train['ensemble_pred']))
print('RMSLE value for train:', rmsle_metric)
mse = mean_squared_error(y_train, predictions_train['ensemble_pred'])
print('MSE value for train', mse)

# Predictions on validation set
predictions_validation = prediction(x_validation)
pred_vs_true_label(true=y_validation, pred=predictions_validation['ensemble_pred'],
                   title='Ensemble - Validation set',
                   save_path='results/prediction_plots/pred_vs_true_ensemble_val.png')
rmsle_metric = sqrt(mean_squared_log_error(y_validation, predictions_validation['ensemble_pred']))
print('RMSLE value for validation', rmsle_metric)
mse = mean_squared_error(y_validation, predictions_validation['ensemble_pred'])
print('MSE value for validation', mse)

# Predictions on test set
predictions_test = prediction(x_test)
predictions_test = np.expm1(predictions_test)
predictions_test = pd.DataFrame(predictions_test)
predictions_test.to_csv('results/prediction_test.tsv', sep='\t', index=False)

test = test.assign(price=predictions_test['ensemble_pred'])
test.to_csv('results/test_with_label_ensemble.tsv', sep='\t', index=False)
