import os
import numpy as np
import joblib
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn import svm

"""
IMPORT DATA:
At the first time, you must run the 'preprocess.py' and 'exploratory_analysis.py' files
"""

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed test data
test = pd.read_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Read numpy data
x_train = np.load(dataset_folder + 'numpy_data/x_train.npy')
y_train = np.load(dataset_folder + 'numpy_data/y_train.npy')
x_test = np.load(dataset_folder + 'numpy_data/x_test.npy')

x_train, x_validation, y_train, y_validation = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

print("SVM REGRESSION")

# Create a svm model
svm_model = svm.LinearSVR(random_state=0, max_iter=3000)

# Train the model using the training set
svm_model.fit(x_train, np.ravel(y_train))

# Save the model
joblib.dump(svm_model, 'models/svm.sav')

# Prediction on test set
y_test = svm_model.predict(x_test)
y_test = np.expm1(y_test)
test = test.assign(price=y_test)
test.to_csv('results/test_with_label_svm.tsv', sep='\t', index=False)
