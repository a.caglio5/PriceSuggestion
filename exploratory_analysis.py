import os
import pandas as pd
import numpy as np

from exploratory_analysis_utilities import distribution_of_continuous_feature, distribution_of_categorical_feature, \
    correlation_matrix
from preprocess_utilities import data_normalization

# Data directory
current_directory = os.getcwd()
dataset_folder = current_directory + '/dataset/'

# Read preprocessed data
train = pd.read_csv(dataset_folder + 'train_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')
test = pd.read_csv(dataset_folder + 'test_preprocessed.tsv', sep='\t', header=0, encoding='ISO-8859-2')

# Divide label and features of train data
x_train = train.drop(columns=['price'])
y_train = pd.DataFrame(train['price'])
x_test = test

"""EXPLORATORY ANALYSIS"""

# Plot distribution of continuous features
distribution_of_continuous_feature('price', y_train, bins=100)
distribution_of_continuous_feature('words_count_name', x_train, bins=20)
distribution_of_continuous_feature('words_count_item_description', x_train, bins=20)

# Plot distribution of categorical features
distribution_of_categorical_feature('brand_name', x_train, top_n=10)
distribution_of_categorical_feature('shipping', x_train)
distribution_of_categorical_feature('item_condition_id', x_train)
distribution_of_categorical_feature('category_name', x_train, top_n=10)

# Removing unused features
x_train = x_train.drop(columns=['train_id', 'name', 'item_description',
                                'category_name', 'category_1', 'category_2',
                                'category_3', 'brand_name'])
x_test = x_test.drop(columns=['test_id', 'name', 'item_description',
                              'category_name', 'category_1', 'category_2',
                              'category_3', 'brand_name'])

# Correlation analysis
correlation_matrix(x_train, y_train)

x_train = x_train.drop(columns=['category_name_encoded'])
x_test = x_test.drop(columns=['category_name_encoded'])

"""DATA NORMALIZATION"""

# Normalization of train and test features
x_train, scaler = data_normalization(x_train)
x_test, _ = data_normalization(x_test, scaler)

# Log-normalization of price (target variable)
y_train['price'] = np.log1p(y_train['price'])
distribution_of_continuous_feature('price', y_train, bins=100)

np.save(dataset_folder + 'numpy_data/x_train', x_train)
np.save(dataset_folder + 'numpy_data/y_train', y_train)
np.save(dataset_folder + 'numpy_data/x_test', x_test)
